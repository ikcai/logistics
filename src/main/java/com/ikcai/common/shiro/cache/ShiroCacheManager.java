package com.ikcai.common.shiro.cache;

import org.apache.shiro.cache.Cache;

/**
 * 标题：
 * 功能：
 *
 * @Author:赵力
 * @Email:mailzhaoli@126.com
 * @Class:com.ikcai.common.shiro.cache.ShiroCacheManager
 * @Date:2017/8/24 15:59
 * @CopyRight:Copyright©2017
 * @Version:1.0
 */
public interface ShiroCacheManager {

    <K, V> Cache<K, V> getCache(String name);

    void destroy();

}
