package com.ikcai.common.shiro.dao;

import com.ikcai.common.permission.bo.UPermissionBo;
import com.ikcai.common.shiro.model.UPermission;

import java.util.List;
import java.util.Set;

/**
 * 标题：权限mapper对象
 * 功能：操作数据库
 *
 * @Author:赵力
 * @Email:mailzhaoli@126.com
 * @Class:com.ikcai.common.shiro.dao.UPermissionMapper
 * @Date:2017/8/24 15:59
 * @CopyRight:Copyright©2017
 * @Version:1.0
 */
public interface UPermissionMapper {
    int deleteByPrimaryKey(Long id);

    int insert(UPermission record);

    int insertSelective(UPermission record);

    UPermission selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UPermission record);

    int updateByPrimaryKey(UPermission record);

    long findCount();

    List<UPermission> findAll();

    List<UPermissionBo> selectPermissionById(Long id);

    //根据用户ID获取权限的Set集合
    Set<String> findPermissionByUserId(Long id);
}