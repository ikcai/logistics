package com.ikcai.common.shiro.dao;


import com.ikcai.common.shiro.model.URolePermission;

import java.util.List;
import java.util.Map;

/**
 * 标题：角色权限mapper对象
 * 功能：操作数据库
 *
 * @Author:赵力
 * @Email:mailzhaoli@126.com
 * @Class:com.ikcai.common.shiro.dao.URolePermissionMapper
 * @Date:2017/8/24 15:59
 * @CopyRight:Copyright©2017
 * @Version:1.0
 */
public interface URolePermissionMapper {
    int insert(URolePermission record);

    int insertSelective(URolePermission record);

    List<URolePermission> findRolePermissionByPid(Long id);

    List<URolePermission> findRolePermissionByRid(Long id);

    List<URolePermission> find(URolePermission entity);

    int deleteByPid(Long id);

    int deleteByRid(Long id);

    int delete(URolePermission entity);

    int deleteByRids(Map<String, Object> resultMap);
}