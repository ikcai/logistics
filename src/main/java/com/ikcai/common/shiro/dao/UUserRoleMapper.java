package com.ikcai.common.shiro.dao;


import com.ikcai.common.shiro.model.UUserRole;

import java.util.List;
import java.util.Map;
/**
 * 标题：用户角色mapper对象
 * 功能：操作数据库
 *
 * @Author:赵力
 * @Email:mailzhaoli@126.com
 * @Class:com.ikcai.common.shiro.dao.UUserRoleMapper
 * @Date:2017/8/24 15:59
 * @CopyRight:Copyright©2017
 * @Version:1.0
 */
public interface UUserRoleMapper {
    int insert(UUserRole record);

    int insertSelective(UUserRole record);

	int deleteByUserId(Long id);

	int deleteRoleByUserIds(Map<String, Object> resultMap);

	List<Long> findUserIdByRoleId(Long id);
}