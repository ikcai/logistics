package com.ikcai.common.shiro.dao;


import com.ikcai.common.permission.bo.RolePermissionAllocationBo;
import com.ikcai.common.shiro.model.URole;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 标题：角色mapper对象
 * 功能：操作数据库
 *
 * @Author:赵力
 * @Email:mailzhaoli@126.com
 * @Class:com.ikcai.common.shiro.dao.URoleMapper
 * @Date:2017/8/24 15:59
 * @CopyRight:Copyright©2017
 * @Version:1.0
 */
public interface URoleMapper {
    int deleteByPrimaryKey(Long id);

    int insert(URole record);

    int insertSelective(URole record);

    URole selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(URole record);

    int updateByPrimaryKey(URole record);

    Set<String> findRoleByUserId(Long id);

    long findCount();

    List<URole> findAll();

    List<URole> findNowAllPermission(Map<String, Object> map);

    List<RolePermissionAllocationBo> findRoleAndPermission();

    void initData();
}