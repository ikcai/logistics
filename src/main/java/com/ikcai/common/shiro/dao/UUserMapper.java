package com.ikcai.common.shiro.dao;


import com.ikcai.common.permission.bo.URoleBo;
import com.ikcai.common.permission.bo.UserRoleAllocationBo;
import com.ikcai.common.shiro.model.UUser;

import java.util.List;
import java.util.Map;

/**
 * 标题：用户mapper对象
 * 功能：操作数据库
 *
 * @Author:赵力
 * @Email:mailzhaoli@126.com
 * @Class:com.ikcai.common.shiro.dao.UUserMapper
 * @Date:2017/8/24 15:59
 * @CopyRight:Copyright©2017
 * @Version:1.0
 */
public interface UUserMapper {
    int deleteByPrimaryKey(Long id);

    int insert(UUser record);

    int insertSelective(UUser record);

    UUser selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UUser record);

    int updateByPrimaryKey(UUser record);

    long findCount();

    List<UUser> findAll();

    UUser login(Map<String, Object> map);

    UUser findUserByEmail(String email);

    List<URoleBo> selectRoleByUserId(Long id);

    List<UserRoleAllocationBo> findUserAndRole();

}