package com.ikcai.common.permission.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ikcai.common.permission.bo.RolePermissionAllocationBo;
import com.ikcai.common.permission.service.RoleService;
import com.ikcai.common.shiro.dao.URoleMapper;
import com.ikcai.common.shiro.dao.URolePermissionMapper;
import com.ikcai.common.shiro.dao.UUserMapper;
import com.ikcai.common.shiro.model.UPermission;
import com.ikcai.common.shiro.model.URole;
import com.ikcai.common.shiro.token.manager.TokenManager;
import com.ikcai.common.utils.LoggerUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
@SuppressWarnings("unchecked")
public class RoleServiceImpl implements RoleService {

    @Autowired
    URoleMapper roleMapper;
    @Autowired
    UUserMapper userMapper;
    @Autowired
    URolePermissionMapper rolePermissionMapper;

    @Override
    public int deleteByPrimaryKey(Long id) {
        return roleMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(URole record) {
        return roleMapper.insert(record);
    }

    @Override
    public int insertSelective(URole record) {
        return roleMapper.insertSelective(record);
    }

    @Override
    public URole selectByPrimaryKey(Long id) {
        return roleMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKey(URole record) {
        return roleMapper.updateByPrimaryKey(record);
    }

    @Override
    public int updateByPrimaryKeySelective(URole record) {
        return roleMapper.updateByPrimaryKeySelective(record);
    }


    @Override
    public List<URole> findPage(Integer pageNo, Integer pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        List<URole> list = roleMapper.findAll();
        PageInfo<URole> pageInfo = new PageInfo<URole>(list);
        return pageInfo.getList();
    }

    @Override
    public List<RolePermissionAllocationBo> findRoleAndPermissionPage(Integer pageNo, Integer pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        List<RolePermissionAllocationBo> list = roleMapper.findRoleAndPermission();
        PageInfo<RolePermissionAllocationBo> pageInfo = new PageInfo<RolePermissionAllocationBo>(list);
        return pageInfo.getList();
    }

    @Override
    public Map<String, Object> deleteRoleById(String ids) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        try {
            int count = 0;
            String resultMsg = "删除成功。";
            String[] idArray = new String[]{};
            if (StringUtils.contains(ids, ",")) {
                idArray = ids.split(",");
            } else {
                idArray = new String[]{ids};
            }

            c:
            for (String idx : idArray) {
                Long id = new Long(idx);
                if (new Long(1).equals(id)) {
                    resultMsg = "操作成功，But'系统管理员不能删除。";
                    continue c;
                } else {
                    count += this.deleteByPrimaryKey(id);
                }
            }
            resultMap.put("status", 200);
            resultMap.put("count", count);
            resultMap.put("resultMsg", resultMsg);
        } catch (Exception e) {
            LoggerUtils.fmtError(getClass(), e, "根据IDS删除用户出现错误，ids[%s]", ids);
            resultMap.put("status", 500);
            resultMap.put("message", "删除出现错误，请刷新后再试！");
        }
        return resultMap;
    }

    @Override
    public Set<String> findRoleByUserId(Long userId) {
        return roleMapper.findRoleByUserId(userId);
    }

    @Override
    public List<URole> findNowAllPermission() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("userId", TokenManager.getUserId());
        return roleMapper.findNowAllPermission(map);
    }

    /**
     * 每20分钟执行一次
     */
    @Override
    public void initData() {
        roleMapper.initData();
    }

}
