package com.ikcai.common.user.service;

import java.util.List;
import java.util.Map;

import com.ikcai.common.permission.bo.URoleBo;
import com.ikcai.common.permission.bo.UserRoleAllocationBo;
import com.ikcai.common.shiro.model.UUser;
import org.springframework.ui.ModelMap;


public interface UUserService {

	int deleteByPrimaryKey(Long id);

	UUser insert(UUser record);

    UUser insertSelective(UUser record);

    UUser selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UUser record);

    int updateByPrimaryKey(UUser record);
    
    UUser login(String email, String pswd);

	UUser findUserByEmail(String email);

	List<UUser> findByPage( Integer pageNo, Integer pageSize);

	Map<String, Object> deleteUserById(String ids);

	Map<String, Object> updateForbidUserById(Long id, Long status);

	List<UserRoleAllocationBo> findUserAndRole(Integer pageNo, Integer pageSize);

	List<URoleBo> selectRoleByUserId(Long id);

	Map<String, Object> addRole2User(Long userId, String ids);

	Map<String, Object> deleteRoleByUserIds(String userIds);
}
