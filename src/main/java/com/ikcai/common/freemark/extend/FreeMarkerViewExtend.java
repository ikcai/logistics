package com.ikcai.common.freemark.extend;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.ikcai.common.shiro.model.UUser;
import com.ikcai.common.shiro.token.manager.TokenManager;
import com.ikcai.common.statics.Constant;
import com.ikcai.common.utils.LoggerUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerView;

/**
 * 标题:
 * 功能:
 *
 * @Author: 赵力
 * @Class: com.ikcai.common.freemark.extend.FreeMarkerViewExtend
 * @Date: 2017/8/23 下午9:10
 * @desc:
 * @Copyright: ikcai.com@2017
 */
public class FreeMarkerViewExtend extends FreeMarkerView {

    protected void exposeHelpers(Map<String, Object> model, HttpServletRequest request){

        try {
            super.exposeHelpers(model, request);
        } catch (Exception e) {
            LoggerUtils.fmtError(FreeMarkerViewExtend.class,e, "FreeMarkerViewExtend 加载父类出现异常。请检查。");
        }
        model.put(Constant.CONTEXT_PATH, request.getContextPath());
        model.putAll(FreeMarker.initMap);
        UUser token = TokenManager.getToken();
        //String ip = IPUtils.getIP(request);
        if(TokenManager.isLogin()){
            model.put("token", token);//登录的token
        }

        model.put("_time", new Date().getTime());
        model.put("NOW_YEAY", Constant.NOW_YEAY);//今年

        model.put("_v", Constant.VERSION);//版本号，重启的时间
        model.put("cdn", "cdn");//CDN域名Constant.DOMAIN_CDN
        model.put("basePath", request.getContextPath());//base目录。

    }
}
